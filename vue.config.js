const path = require('path');
module.exports = {
    "publicPath": "",
    "pluginOptions": {
        "cordovaPath": "src-cordova"
    },
    "transpileDependencies": [
        "vuetify"
    ],
    chainWebpack: config => {
        config.resolve.alias
            .set('icons', path.resolve(__dirname, 'node_modules/vue-material-design-icons'));
    }
}