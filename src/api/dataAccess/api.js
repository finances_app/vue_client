import superagent from 'superagent'
import Result from '../entity/ApiResult'
import _ from 'lodash'

/**
 * Abstraction of api calls
 */
const api = {
    /**
     * @param {object} config
     * @param {string} config.bearer
     * @param {'GET' | 'POST' | 'PUT' | 'DELETE'} config.method
     * @param {string} config.url
     * @param {object} [config.data]
     * @returns {Promise<Result>}
     */
    async request(config) {
        const { bearer, method, url, data } = config
        const timerStart = Date.now()

        try {
            let res = await superagent(method, url)
                .send(data)
                .set('Authorization', `Bearer ${bearer}`)
                .set('Cache-Control', 'no-cache') // prevent 304 responses
            return this._handleResponse(res, timerStart)
        } catch (err) {
            return this._handleError(err, timerStart)
        }
    },

    _handleResponse(res, timerStart) {
        const body = _.omit(res.body, ['pendingCacheEvents'])
        const expirationEvents = res.body.pendingCacheEvents

        return new Result({
            status: res.status,
            body,
            expirationEvents,
            time: Date.now() - timerStart,
        })
    },

    _handleError(err, timerStart) {
        if (err.status) {
            // handle server-side error
            return new Result({
                status: err.status,
                validationError: err.status === 400 && _.get(err, 'response.body.message', '') || '',
                error: err,
                time: Date.now() - timerStart,
            })
        } else {
            // handle client-side error
            // could be: timeout, aborded request, malformed config, unreachable or generic exception
            return new Result({
                error: err,
                time: Date.now() - timerStart,
            })
        }
    }
}

export {
    api
}
