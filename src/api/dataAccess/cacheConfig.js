import _ from 'lodash'

class ConfigEntry {
    constructor(data) {
        let { pos, RE, description, expiration } = data;
        this.pos = pos;
        this.RE = RE;
        this.expiration = expiration || null;
        this.description = description;
        this.expires = [];
    }
}



const SECTION = {
    OBJECT_ID: `[0-9a-f]+`,
    BOOL: `(false|true)`,
    INT: `[0-9]+`,
    WORDS: `.+`,
    DATE: `[0-9]{4}-[0-9]{2}-[0-9]{2}`,
    YEAR: `[0-9]{4}`,
};
// Magical variable used to reorder the object properties in the same order they are declared here latter !
let pos = 0;
/*
 * Instructions to implement caching to a new route :
 * 1. Collect underpants
 * 2. Add a new property to this.config with the format
 * {
        pos: pos++,
        description: `A simple human readable description of the route's usage`,
        RE: new RegExp(`^METHOD/complete/url/without/query/params`),
 *  }
 *  3. Make sure its placed at the right place ! The RE are tested in order of declaration !
 *  4. Clear cache data next time the app is used
 *  5. Profit
 */
const config = {
    POST__file: new ConfigEntry({
        pos: pos++,
        description: 'Upload un fichier',
        RE: new RegExp(`^POST/file$`),
    }),

    GET__api__charts__spending: new ConfigEntry({
        pos: pos++,
        description: 'Valeur calculé du diagram des dépenses',
        RE: new RegExp(`^GET/api/charts/spending$`),
    }),
    GET__api__charts__bar__$from_date__$to_date__$group_period: new ConfigEntry({
        pos: pos++,
        description: 'Données bar chart résumé des catégories par période',
        RE: new RegExp(`^GET/api/charts/bar/${SECTION.DATE}/${SECTION.DATE}/[Mty]$`),
    }),

    GET__api__transaction__recent: new ConfigEntry({
        pos: pos++,
        description: 'Historique des transactions',
        RE: new RegExp(`^GET/api/transaction/recent$`),
    }),
    GET__api__transaction__statement__$statement_id: new ConfigEntry({
        pos: pos++,
        description: "Obtient les transactions d'un relevé",
        RE: new RegExp(`^GET/api/transaction/statement/${SECTION.OBJECT_ID}$`),
    }),
    GET__api__transaction__account__$statement_id: new ConfigEntry({
        pos: pos++,
        description: "Obtient les transactions effectué avec un compte pour le relevé en cours",
        RE: new RegExp(`^GET/api/transaction/account/${SECTION.WORDS}$`),
    }),
    GET__api__transaction__pending__$statement_id: new ConfigEntry({
        pos: pos++,
        description: "Obtient les transactions en attente pour une carte de crédit",
        RE: new RegExp(`^GET/api/transaction/pending/${SECTION.WORDS}$`),
    }),
    GET__api__transaction__category__$category_id: new ConfigEntry({
        pos: pos++,
        description: "Obtient les transactions d'une catégorie",
        RE: new RegExp(`^GET/api/transaction/category/${SECTION.OBJECT_ID}$`),
    }),
    GET__api__transaction__$transaction_id: new ConfigEntry({
        pos: pos++,
        description: 'Obtient les transactions du relevé avec la même description',
        RE: new RegExp(`^GET/api/transaction/${SECTION.OBJECT_ID}$`),
    }),
    PUT__api__transaction__$transaction_id: new ConfigEntry({
        pos: pos++,
        description: 'Assigne une catégorie à un groupe de transactions',
        RE: new RegExp(`^PUT/api/transaction/${SECTION.OBJECT_ID}$`),
    }),

    GET__api__statement__approved__$is_approved: new ConfigEntry({
        pos: pos++,
        description: 'Obtient les relevés approuvés ou non',
        RE: new RegExp(`^GET/api/statement/approved/${SECTION.BOOL}$`),
    }),
    GET__api__statement__history: new ConfigEntry({
        pos: pos++,
        description: 'Historique des relevés',
        RE: new RegExp(`^GET/api/statement/history$`),
    }),
    GET__api__statement__count: new ConfigEntry({
        pos: pos++,
        description: 'number of unapproved statements',
        RE: new RegExp(`^GET/api/statement/count$`),
    }),
    GET__api__statement__$statement_id: new ConfigEntry({
        pos: pos++,
        description: 'Obtient un relevé',
        RE: new RegExp(`^GET/api/statement/${SECTION.OBJECT_ID}$`),
    }),
    DELETE__api__statement__$statement_id: new ConfigEntry({
        pos: pos++,
        description: 'Supprimer un relevé',
        RE: new RegExp(`^DELETE/api/statement/${SECTION.OBJECT_ID}$`),
    }),
    PUT__api__statement__$statement_id__approve: new ConfigEntry({
        pos: pos++,
        description: 'Approuver un relevé',
        RE: new RegExp(`^PUT/api/statement/${SECTION.OBJECT_ID}/approve$`),
    }),
    POST__api__statement__file__$file_type: new ConfigEntry({
        pos: pos++,
        description: 'Importer un relevé',
        RE: new RegExp(`^POST/api/statement/file/${SECTION.INT}$`),
    }),

    GET__api__category__count: new ConfigEntry({
        pos: pos++,
        description: 'Number of sub-category without a parent category',
        RE: new RegExp(`^GET/api/category/count$`),
    }),
    GET__api__category__trends__$category_id: new ConfigEntry({
        pos: pos++,
        description: 'Spending averages for a category',
        RE: new RegExp(`^GET/api/category/trends/${SECTION.OBJECT_ID}$`),
    }),
    GET__api__category__$category_type: new ConfigEntry({
        pos: pos++,
        description: 'Obtient des catégories par type',
        RE: new RegExp(`^GET/api/category/${SECTION.INT}$`),
    }),
    GET__api__category__tree: new ConfigEntry({
        pos: pos++,
        description: `Obtient les catégories sous forme d'arbre`,
        RE: new RegExp(`^GET/api/category/tree$`),
    }),
    GET__api__category__last_activity: new ConfigEntry({
        pos: pos++,
        description: 'Get latest activity date of each categories',
        RE: new RegExp(`^GET/api/category/last_activity$`),
    }),
    GET__api__category: new ConfigEntry({
        pos: pos++,
        description: 'Obtient toutes les catégories',
        RE: new RegExp(`^GET/api/category$`),
    }),
    POST__api__category: new ConfigEntry({
        pos: pos++,
        description: 'Ajouter ou modifier une catégorie',
        RE: new RegExp(`^POST/api/category$`),
    }),
    DELETE__api__category__$category_id: new ConfigEntry({
        pos: pos++,
        description: 'Supprime une catégorie',
        RE: new RegExp(`^DELETE/api/category/${SECTION.OBJECT_ID}$`),
    }),

    GET__api__search: new ConfigEntry({
        pos: pos++,
        description: 'Regroupe toutes les requete de recherche',
        RE: new RegExp(`^GET/api/search`),
    }),

    GET__api__account__balance: new ConfigEntry({
        pos: pos++,
        description: 'Balances actuelle des comptes',
        RE: new RegExp(`^GET/api/account/balance$`),
    }),

    ANY__api__task_or_utilities: new ConfigEntry({
        pos: pos++,
        description: "Englobe tous les routes du controlleur task et utilities",
        RE: new RegExp(`^(GET|POST|PUT|DELETE)/api/(task|utilities)`),
    }),

    GET__api__budget__$year__category_summary: new ConfigEntry({
        pos: pos++,
        description: 'Obtient le résumé annuel des dépenses et revenues par catégorie racine.',
        RE: new RegExp(`^GET/api/budget/${SECTION.YEAR}/category_summary$`),
    }),
    GET__api__budget__$year__category_summary__$category_id: new ConfigEntry({
        pos: pos++,
        description: 'Obtient le résumé annuel des dépenses et revenues pour la catégorie.',
        RE: new RegExp(`^GET/api/budget/${SECTION.YEAR}/category_summary/${SECTION.OBJECT_ID}$`),
    }),
    GET__api__budget__$year__status: new ConfigEntry({
        pos: ++pos,
        description: 'Get the budget entry statuses for the current period.',
        RE: new RegExp(`^GET/api/budget/${SECTION.YEAR}/status$`),
    }),
    GET__api__budget__$year: new ConfigEntry({
        pos: pos++,
        description: 'Obtiens les catégories sous forme d\'arbre et les budgets pour ces catégories.',
        RE: new RegExp(`^GET/api/budget/${SECTION.YEAR}$`),
    }),
    POST__api__budget__$year: new ConfigEntry({
        pos: pos++,
        description: 'Initialise un budget annuel.',
        RE: new RegExp(`^POST/api/budget/${SECTION.YEAR}$`),
    }),
    PUT__api__budget__$year__category__$category_id: new ConfigEntry({
        pos: pos++,
        description: 'Update a budget entry',
        RE: new RegExp(`^PUT/api/budget/${SECTION.YEAR}/category/${SECTION.OBJECT_ID}$`),
    }),
    DELETE__api__budget__$year__category__$category_id: new ConfigEntry({
        pos: pos++,
        description: 'Delete a budget entry',
        RE: new RegExp(`^DELETE/api/budget/${SECTION.YEAR}/category/${SECTION.OBJECT_ID}$`),
    }),
    GET__api__budget__line_date__observed__$from_date__to__$to_date: new ConfigEntry({
        pos: pos++,
        description: 'Donnnés pour la série d\'historique du solde du graphique.',
        RE: new RegExp(`^GET/api/budget/line_data/observed/${SECTION.DATE}/to/${SECTION.DATE}$`),
    }),
    GET__api__budget__line_date__planned__$from_date__to__$to_date: new ConfigEntry({
        pos: pos++,
        description: 'Données pour la série de prévision du solde selon le budget.',
        RE: new RegExp(`^GET/api/budget/line_data/planned/${SECTION.DATE}/to/${SECTION.DATE}$`),
    }),
    GET__api__budget__line_date__planned_adjusted__$from_date__to__$to_date: new ConfigEntry({
        pos: pos++,
        description: `Données pour la série de prévision ajusté avec l'historique du solde du solde selon le budget.`,
        RE: new RegExp(`^GET/api/budget/line_data/planned_adjusted/${SECTION.DATE}/to/${SECTION.DATE}$`),
    }),
    GET__api__budget__planned_transactions__$from_date__to__$to_date: new ConfigEntry({
        pos: pos++,
        description: 'Fetches the planned transactions for a specific period',
        RE: new RegExp(`^GET/api/budget/planned_transactions/${SECTION.DATE}/to/${SECTION.DATE}$`),
    }),

    //GET/api/user/devices
    GET__api__user__devices: new ConfigEntry({
        pos: pos++,
        description: 'Obtient l\'historique d\'ip et appareil qui s\'est connecté au compte.',
        RE: new RegExp(`^GET/api/user/devices$`),
    }),
};

let values = [];
_.each(config, (value, key) => {
    values.push({ key, value });
});
const configValues = values.sort((a,b) => {
    if (a.value.pos < b.value.pos) {
        return -1;
    } else if (a.value.pos > b.value.pos) {
        return 1;
    } else {
        return 0;
    }
});



const routeGroups = {
    statement: [
        config.GET__api__statement__count,
        config.GET__api__statement__history,
        config.GET__api__statement__approved__$is_approved,
        config.GET__api__statement__$statement_id,
    ],
    transaction: [
        config.GET__api__transaction__statement__$statement_id,
        config.GET__api__transaction__account__$statement_id,
        config.GET__api__transaction__$transaction_id,
        config.GET__api__transaction__recent,
        config.GET__api__budget__line_date__observed__$from_date__to__$to_date,
    ],
    category: [
        config.GET__api__category,
        config.GET__api__category__$category_type,
        config.GET__api__category__tree,
        config.GET__api__category__count,
    ],
    scrapingData: [
        config.GET__api__account__balance, // This one is a bit special, cause no client side action
                                                // can expire this one.
    ],
    budgetRelatedTransactionCategoryAggregate: [
        config.GET__api__budget__$year__category_summary,
        config.GET__api__budget__$year__category_summary__$category_id,
        config.GET__api__budget__line_date__planned__$from_date__to__$to_date,
        config.GET__api__budget__line_date__planned_adjusted__$from_date__to__$to_date,
        config.GET__api__budget__$year, // This one is indirectly affected by transaction, because the
                                             // category activity history is used in there.
        config.GET__api__budget__$year__status,
        config.GET__api__budget__planned_transactions__$from_date__to__$to_date,
    ],
    transactionCategoryAggregate: [
        config.GET__api__category__last_activity,
        config.GET__api__charts__spending,
        config.GET__api__charts__bar__$from_date__$to_date__$group_period,
        config.GET__api__budget__$year__category_summary,
        config.GET__api__budget__$year__category_summary__$category_id,
        config.GET__api__budget__line_date__planned__$from_date__to__$to_date,
        config.GET__api__budget__line_date__planned_adjusted__$from_date__to__$to_date,
        config.GET__api__budget__$year,
        config.GET__api__budget__$year__status,
    ],
};

config.PUT__api__transaction__$transaction_id.expires = [
    ...routeGroups.transaction,
    ...routeGroups.category,
    ...routeGroups.transactionCategoryAggregate,
];
config.DELETE__api__statement__$statement_id.expires = [
    ...routeGroups.statement,
    ...routeGroups.transaction,
    ...routeGroups.transactionCategoryAggregate,
];

config.PUT__api__budget__$year__category__$category_id.expires = [
    ...routeGroups.budgetRelatedTransactionCategoryAggregate,
    config.GET__api__budget__planned_transactions__$from_date__to__$to_date,
];
config.DELETE__api__budget__$year__category__$category_id.expires = [
    ...routeGroups.budgetRelatedTransactionCategoryAggregate,
    config.GET__api__budget__planned_transactions__$from_date__to__$to_date,
];

config.POST__api__category.expires = [
    ...routeGroups.transaction,
    ...routeGroups.category,
    ...routeGroups.transactionCategoryAggregate,
];
config.DELETE__api__category__$category_id.expires = [
    config.GET__api__statement__count,
    ...routeGroups.transaction,
    ...routeGroups.category,
    ...routeGroups.transactionCategoryAggregate,
];

config.POST__api__budget__$year.expires = [
    config.GET__api__budget__$year__category_summary,
];
config.PUT__api__statement__$statement_id__approve.expires = [
    ...routeGroups.statement
];
// imports new transactions, but the server generates expiration events for those.
config.POST__api__statement__file__$file_type.expires = [
    ...routeGroups.statement
];



const cudUrlTypes = [
    config.POST__file,
    config.DELETE__api__statement__$statement_id,
    config.PUT__api__statement__$statement_id__approve,
    config.POST__api__statement__file__$file_type,
    config.PUT__api__transaction__$transaction_id,
    config.POST__api__category,
    config.DELETE__api__category__$category_id,
    config.POST__api__budget__$year,
    config.PUT__api__budget__$year__category__$category_id,
    config.DELETE__api__budget__$year__category__$category_id,
];
const ignoredTypes = [
    config.GET__api__search,
    config.GET__api__transaction__category__$category_id,
    config.GET__api__transaction__pending__$statement_id,
    config.GET__api__category__trends__$category_id,
    config.ANY__api__task_or_utilities,
    config.GET__api__user__devices,
];



const serverEventExpirationMap = {
    'TRANSACTIONS_IMPORTED': [
        ...routeGroups.scrapingData,
        config.GET__api__statement__count,
        config.GET__api__statement__history,
        config.GET__api__transaction__statement__$statement_id,
        config.GET__api__transaction__account__$statement_id,
        config.GET__api__category__last_activity
    ],
    'NON_TRANSFER_TRANSACTIONS_IMPORTED': [
        ...routeGroups.scrapingData,
        config.GET__api__statement__count,
        config.GET__api__statement__history,
        ...routeGroups.transaction,
        ...routeGroups.transactionCategoryAggregate
    ],
    'STATEMENT_UNAPPROVED': [
        ...routeGroups.statement
    ],
}



/**
 * Describe the relation between every api resources to allow cache expiration.
 */
export {
    configValues,
    cudUrlTypes,
    ignoredTypes,
    serverEventExpirationMap,
}
