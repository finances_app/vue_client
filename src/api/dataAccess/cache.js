import * as ms from 'ms'
import Pouchdb from 'pouchdb'
import PouchdbFind from 'pouchdb-find'
import { api } from './api'
import {
    configValues,
    cudUrlTypes,
    ignoredTypes,
    serverEventExpirationMap
} from './cacheConfig'

Pouchdb.plugin(PouchdbFind)

const cacheDb = new Pouchdb('cache')
cacheDb
    .createIndex({
        index: {
            fields: ['type'],
        },
    })
    .catch(err => console.warn(`Error creating indexes (${err})`))

/**
 * Middleman between the api and the data controller.
 * Intecept data when an api call is made, keeps track of expired data and shortcuts the api calls
 * if fresh data is available localy.
 *
 * TODO: implement the automatic cache update
 */
export default {
    /**
     * @param {object} config
     * @param {string} config.bearer
     * @param {'GET' | 'POST' | 'PUT' | 'DELETE'} config.method
     * @param {string} config.url
     * @param {object} [config.data]
     * @param {boolean} config.acceptExpired
     * @returns {Promise<Result>}
     */
    async request(config) {
        const truncatedUrl = config.url.split('?').shift()
        const urlType = this._getUrlType(config.method, truncatedUrl)
        const isActionRequest = cudUrlTypes.includes(urlType)
        const canBeCached = urlType !== null && !isActionRequest && !ignoredTypes.includes(urlType)

        if (canBeCached) {

            const cachedResult = await this._getCacheEntry(config)
            if (cachedResult) {
                cachedResult.fromCache = true
                return cachedResult
            }

            const result = await this._apiRequest(config)
            if (result.isSuccess) {
                this._setCache(urlType, config.method + config.url, result)
            }
            return result;

        } else if (isActionRequest) {

            const result = await this._apiRequest(config)
            if (result.isSuccess) {
                this._expireCache(urlType.expires)
            }
            return result;

        } else {
            // un-configured request, hand it to the api directly
            return await this._apiRequest(config)
        }
    },

    async _apiRequest(config) {
        const result = await api.request(config)
        await Promise.all(
            result.expirationEvents.map(_event => this._expireFromServerEvent(_event))
        )
        return result
    },

    /**
     * Expire cache entries depending on the server event
     * @param {string} eventType
     * @returns {Promise<void>}
     */
    async _expireFromServerEvent(eventType) {
        let urlTypesToExpire = serverEventExpirationMap[eventType] || []
        try {
            await this._expireCache(urlTypesToExpire);
        } catch (err) {
            console.warn(err);
        }
    },

    async _getCacheEntry(config) {
        try {
            let entry = await cacheDb.get(config.method + config.url)
            if (entry && entry.expiration === null || entry.expiration > Date.now() || config.acceptExpired) {
                return JSON.parse(entry.response)
            }
            return null
        } catch (err) {
            return null
        }
    },

    /**
     * Get the url type
     * @param method {"GET" | "POST" | "PUT" | "DELETE"}
     * @param url {string}
     * @return {?ConfigEntry}
     * @private
     */
    _getUrlType(method, url) {
        for (let index = 0; index < configValues.length; index++) {
            let regexp = configValues[index].value.RE;
            if (regexp.test(`${method}${url}`)) {
                return configValues[index].value;
            }
        }
        console.warn(`No match found for url '${method}${url}'`);
        return null;
    },

    /**
     * Update the value of a cache entry
     * @param urlType {ConfigEntry}
     * @param combinedUrl {string}
     * @param data {*}
     * @private
     */
    async _setCache(urlType, combinedUrl, data) {
        // remove existing entry if present
        try {
            let entry = await cacheDb.get(combinedUrl);
            await cacheDb.remove(entry);
        } catch (err) {
            /* do nothing */
        }

        await cacheDb.put({
            _id: combinedUrl,
            type: urlType.pos,
            response: JSON.stringify(data),
            expiration: urlType.expiration ? Date.now() + urlType.expiration : null,
        });
    },

    /**
     * Expire cache entries related to that url
     * @param {ConfigEntry[]} urlTypesToExpire
     * @private
     */
    async _expireCache(urlTypesToExpire) {
        let filteredDocs = await cacheDb.find({
            selector: {
                type: { $in: urlTypesToExpire.map(type => type.pos) },
            },
        });
        console.info(filteredDocs);
        // ctrl.logCtrl.log(`<span style="color:#f4d442">Expired ${filteredDocs.docs.length} cache entries</span>`);
        await Promise.all(_.each(filteredDocs.docs, async entry => {
            entry.expiration = Date.now() - ms('1h');
            await cacheDb.put(entry);
        }));
    },
}
