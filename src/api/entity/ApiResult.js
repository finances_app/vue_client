export default class Result {
    get isSuccess() {
        return 200 <= this.status && this.status < 400
    }

    get hasValidationError() {
        return this.status === 400
    }

    get isFromCache() {
        return this.fromCache
    }

    get isNetworkError() {
        return !this.status && this.error && ['Request has been terminated'].some(_message => {
            return this.error.message.includes(_message)
        })
    }

    /**
     * @param {object} config
     * @param {number} [config.status]
     * @param {object} config.body
     * @param {string} [config.validationError]
     * @param {string[]} [config.expirationEvents]
     * @param {Error} [config.error]
     * @param {boolean} config.fromCache
     * @param {number} config.time
     */
    constructor(config) {
        const { status, body, validationError, expirationEvents, error, time, fromCache } = config

        this.status = status
        this.body = body
        this.validationError = validationError
        this.expirationEvents = expirationEvents || []
        this.error = error
        this.fromCache = fromCache
        this.time = time
    }
}
