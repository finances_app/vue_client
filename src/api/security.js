// api calls related to the security
// - authentication
// - server ping
// - device and location history
import superagent from 'superagent'
import Result from './entity/ApiResult'

export default {
    /**
     * Authenticate an id_token provided by Google Sign-in service
     * @param idToken {string}
     * @param userId {string}
     * @returns {Promise<Result>}
     */
    async OAuthLogin(idToken, userId) {
        let timerStart = window.performance.now()

        try {
            const res = await superagent('POST', '/api/authenticate')
                .send({
                    id_token: idToken,
                    user_id: userId
                })

            // await this._handleCacheExpiration(res.data.pendingCacheEvents)
            // this._logResponse(config, res, window.performance.now() - start)

            return this._handleResponse(res, timerStart)
        } catch (err) {
            return this._handleError(err, timerStart)
        }
    },

    /**
     * Authenticate the user with a username-password pair
     * @param {string} username
     * @param {string} password
     * @returns {Promise<Result>}
     */
    async passwordLogin(username, password) {
        // TODO: actualy implement this server side, using a dedicated route and all

        // .. for now, use this for the demo user and browser login bypass
        return await this.OAuthLogin(password, username)
    },

    _handleResponse(res, timerStart) {
        const body = _.omit(res.body, ['pendingCacheEvents'])
        const expirationEvents = res.body.pendingCacheEvents

        return new Result({
            status: res.status,
            body,
            expirationEvents,
            time: Date.now() - timerStart,
        })
    },

    _handleError(err, timerStart) {
        if (err.status) {
            // handle server-side error
            return new Result({
                status: err.status,
                validationError: err.status === 400 && _.get(err, 'response.body.message', '') || '',
                error: err,
                time: Date.now() - timerStart,
            })
        } else {
            // handle client-side error
            // could be: timeout, aborded request, malformed config, unreachable or generic exception
            return new Result({
                error: err,
                time: Date.now() - timerStart,
            })
        }
    }
}
