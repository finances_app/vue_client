import api from './dataAccess/cache'
import Result from './entity/ApiResult'
import store from '../store'
import _ from 'lodash'

/**
 * Contains a maping of every API routes to async / await function.
 */
export default {
    /**
     * @param {object} config
     * @param {'GET' | 'POST' | 'PUT' | 'DELETE'} config.method
     * @param {string} config.url
     * @param {object} [config.data]
     * @param {boolean} [config.acceptExpired]
     * @returns {Promise<Result>}
     */
    async _request(config) {
        config = _.extend(config, {
            bearer: store.state.security.accessToken
        })

        let res = await api.request(config)
        console.log(res)
        const result = new Result(res)

        if (result.hasValidationError) {
            store.mutations.showSnackbar({
                message: result.validationError
            })

        } else if (result.isNetworkError) {

            store.mutations.setOnlineState(false)

        } else if (!result.isSuccess) {

            store.mutations.showSnackbar({
                message: `[${result.status}] ${result.error.message}`,
                isError: true
            })
        }

        return result
    },

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// SECURITY CONTROLLER
    ///

    /**
     * @returns {Promise<Result>}
     */
    async getDeviceHistory() {
        return await this._request({
            method: 'GET',
            url: '/api/user/devices',
        })
    },

    /**
     * @returns {Promise<Result>}
     */
    async getLocationHistory() {
        return await this._request({
            method: 'GET',
            url: '/api/user/locations',
        })
    },

    /**
     * @returns {Promise<Result>}
     */
    async deauthorizeSessions() {
        return await this._request({
            method: 'PUT',
            url: '/api/user/deauthorize',
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// STATEMENT CONTROLLER
    ///

    /**
     * @param fileType {number}
     * @param file {File}
     * @returns {Promise<Result>}
     */
    async uploadStatement(fileType, file) {
        let formData = new FormData()
        formData.set('file', file)
        return await this._request({
            method: 'POST',
            url: `/api/statement/file/${fileType}`,
            data: formData,
        })
    },

    /**
     * @returns {Promise<Result>}
     */
    async getUnapprovedStatements() {
        return await this._request({
            method: 'GET',
            url: `/api/statement/approved/false`,
        })
    },

    /**
     * @param size {number}
     * @param skip {number}
     * @returns {Promise<Result>}
     */
    async getImportHistory(size, skip) {
        let url = '/api/statement/history'
        if (size) {
            url += `?size=${size}`
            if (skip) {
                url += `&skip=${skip}`
            }
        }

        return await this._request({
            method: 'GET',
            url: url,
        })
    },

    /**
     * @param statementId {string}
     * @returns {Promise<Result>}
     */
    async approveStatement(statementId) {
        return await this._request({
            method: 'PUT',
            url: `/api/statement/${statementId}/approve`,
        })
    },

    /**
     * @param statementId {string}
     * @returns {Promise<Result>}
     */
    async deleteStatement(statementId) {
        return await this._request({
            method: 'DELETE',
            url: `/api/statement/${statementId}`,
        })
    },

    /**
     * @param statementId
     * @returns {Promise<Result>}
     */
    async getStatement(statementId) {
        return await this._request({
            method: 'GET',
            url: `/api/statement/${statementId}`,
        })
    },

    /**
     * @returns {Promise<Result>}
     */
    async getUnapprovedStatementCount() {
        return await this._request({
            method: 'GET',
            url: `/api/statement/count`,
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// TRANSACTION CONTROLLER
    ///

    /**
     * @param statementId {string}
     * @returns {Promise<Result>}
     */
    async getDistinctTransactionForStatement(statementId) {
        return await this._request({
            method: 'GET',
            url: `/api/transaction/statement/${statementId}?unique_description=true`,
        })
    },

    /**
     * @param statementId {string}
     * @returns {Promise<Result>}
     */
    async getTransactionsByStatement(statementId) {
        return await this._request({
            method: 'GET',
            url: `/api/transaction/statement/${statementId}?include_category_hierarchy=false`,
        })
    },

    /**
     * @param {string} categoryId
     * @param {Date} from
     * @param {Date} to
     * @returns {Promise<Result>}
     */
    async getTransactionsByCategory(categoryId, from, to) {
        return await this._request({
            method: 'GET',
            url: `/api/transaction/category/${categoryId}?from=${from.getTime()}&to=${to.getTime()}&include_category_hierarchy=true`,
        })
    },

    /**
     * @param {string} accountId
     * @returns {Promise<Result>}
     */
    async getTransactionsByAccount(accountId) {
        return await this._request({
            method: 'GET',
            url: `/api/transaction/account/${accountId}`
        })
    },

    /**
     * @param {string} accountId
     * @returns {Promise<Result>}
     */
    async getPendingTransactions(accountId) {
        return await this._request({
            method: 'GET',
            url: `/api/transaction/pending/${accountId}`
        })
    },

    /**
     * @param transactionId
     * @returns {Promise<Result>}
     */
    async getSubCategoryCorrectionViewData(transactionId) {
        return await this._request({
            method: 'GET',
            url: `/api/transaction/${transactionId}?forSubCategoryAssignment=true`,
        })
    },

    /**
     * @param transactionId {string}
     * @param categoryName {string}
     * @param systemType {number}
     * @returns {Promise<Result>}
     */
    async assignSubCategory(transactionId, categoryName, systemType) {
        return await this._request({
            method: 'PUT',
            url: `/api/transaction/${transactionId}`,
            data: {
                category_name: categoryName,
                system_type: systemType,
            },
        })
    },

    /**
     * @param size {?number}
     * @param skip {?number}
     * @param ignoreTransfer {?boolean}
     * @returns {Promise<Result>}
     */
    async getRecentTransactions(size = 10, skip = 0, ignoreTransfer = false) {
        let url = `/api/transaction/recent?size=${size}&skip=${skip}&include_category_hierarchy=true`
        if (ignoreTransfer) {
            url += '&ignore_transfer=true'
        }

        return await this._request({
            method: 'GET',
            url: url,
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// CATEGORY CONTROLLER
    ///

    /**
     * @param type {?number}
     * @param includeChildren {boolean}
     * @returns {Promise<Result>}
     */
    async getCategories(type = null, includeChildren = false) {
        let url = '/api/category'
        if (type) {
            url += `/${type}`
            if (includeChildren) {
                url += `?include_children`
            }
        }

        return await this._request({
            method: 'GET',
            url: url,
        })
    },

    /**
     * @param {string} name
     * @returns {Promise<Result>}
     */
    async getCategoryByName(name) {
        let res = await this._request({
            method: 'GET',
            url: `/api/category?name=${encodeURIComponent(name)}`
        })
        if (res && res.categories && res.categories.length === 1) {
            return res.categories[0]
        } else {
            return null
        }
    },

    /**
     * @returns {Promise<Result>}
     */
    async getCategoryTree() {
        return await this._request({
            method: 'GET',
            url: `/api/category/tree`,
        })
    },

    /**
     * Map of the last activity (transaction being assigned to the category) for every categories.
     * If no date range is provided, default to activity within the last 365+ days.
     * The object has key value pairs of <categoryId:date>
     * @param {object} [options]
     * @param {string} options.bearer
     * @param {Date} [options.from]
     * @param {Date} [options.to]
     * @returns {Promise<Result>}
     */
    async getCategoryLastActivity(options = {}) {
        // To prevent too many cache misses, rounding up start and end of this year period to the trimester.
        let trimester = Math.floor(dayjs().month() / 4)
        let startOfTrimester = dayjs().month(trimester * 4).startOf('month')
        let from = options.from || startOfTrimester.clone().subtract(1, 'year')
        let to = options.to || startOfTrimester.clone().add(3, 'month').endOf('month')

        return await this._request({
            method: 'GET',
            url: `/api/category/last_activity?from=${from.format('YYYY-MM-DD')}&to=${to.format('YYYY-MM-DD')}`,
        })
    },

    /**
     * @param category {ns.App.Models.Category}
     * @returns {Promise<Result>}
     */
    async saveCategory(category) {
        return await this._request({
            method: 'POST',
            url: '/api/category',
            data: category,
        })
    },

    /**
     * @param categoryId {string}
     * @returns {Promise<Result>}
     */
    async deleteCategory(categoryId) {
        return await this._request({
            method: 'DELETE',
            url: `/api/category/${categoryId}`,
        })
    },

    /**
     * @returns {Promise<Result>}
     */
    async getUnassignedBaseCategoryCount() {
        return await this._request({
            method: 'GET',
            url: `/api/category/count`,
        })
    },

    /**
     * @param {string} categoryId
     * @returns {Promise<Result>}
     */
    async getCategorySpendingTrends(categoryId) {
        return await this._request({
            method: 'GET',
            url: `/api/category/trends/${categoryId}`,
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// BUDGET CONTROLLER
    ///

    /**
     * @param year
     * @param {object} options
     * @param {Date} [options.from]
     * @param {Date} [options.to]
     * @param {string} [options.categoryId]
     * @returns {Promise<Result>}
     */
    async getBudgetCategorySummary(year, options) {
        let url = `/api/budget/${year}/category_summary`
        if (options.categoryId) {
            url += `/${options.categoryId}`
        }

        let query = []
        if (options.from) {
            query.push(`from=${options.from.toLocaleDateString('fr-CA')}`)
        }
        if (options.to) {
            query.push(`to=${options.to.toLocaleDateString('fr-CA')}`)
        }
        if (!_.isEmpty(query)) {
            url += '?' + query.join('&')
        }

        return await this._request({
            method: 'GET',
            url: url,
        })
    },

    /**
     * @param {dayjs.Dayjs} from
     * @param {dayjs.Dayjs} to
     * @returns {Promise<Result>}
     */
    async getBudgetObservedLineData(from, to) {
        return await this._request({
            method: 'GET',
            url: `/api/budget/line_data/observed/${from.format('YYYY-MM-DD')}/to/${to.format('YYYY-MM-DD')}`
        })
    },

    /**
     * @param {dayjs.Dayjs} from
     * @param {dayjs.Dayjs} to
     * @returns {Promise<Result>}
     */
    async getBudgetPlannedLineData(from, to) {
        return await this._request({
            method: 'GET',
            url: `/api/budget/line_data/planned/${from.format('YYYY-MM-DD')}/to/${to.format('YYYY-MM-DD')}`
        })
    },

    /**
     * @param {dayjs.Dayjs} from
     * @param {dayjs.Dayjs} to
     * @returns {Promise<Result>}
     */
    async getBudgetPlannedAdjustedLineData(from, to) {
        return await this._request({
            method: 'GET',
            url: `/api/budget/line_data/planned_adjusted/${from.format('YYYY-MM-DD')}/to/${to.format('YYYY-MM-DD')}`
        })
    },

    /**
     * @param {string[]} categoryIds
     * @returns {Promise<Result>}
     */
    async getBudgetStatus(categoryIds) {
        return await this._request({
            method: 'GET',
            url: `/api/budget/${dayjs().year()}/status?categories=${categoryIds.join(',')}`,
        })
    },

    /**
     * @param {number} year
     * @returns {Promise<Result>}
     */
    async getBudget(year) {
        return await this._request({
            method: 'GET',
            url: `/api/budget/${year}`,
        })
    },

    /**
     * @param {number} year
     * @returns {Promise<Result>}
     */
    async createBudget(year) {
        return await this._request({
            method: 'POST',
            url: `/api/budget/${year}`,
        })
    },

    /**
     * @param {number} year
     * @param {string} categoryId
     * @param {object} entry
     * @param {boolean} entry.isComplex
     * @param {number} [entry.amount]
     * @param {string} [entry.period]
     * @param {{ amount: number, startDate: Date, period: string, endDate: Date }[]} [entry.rules]
     * @returns {Promise<Result>}
     */
    async saveBudgetEntry(year, categoryId, entry) {
        return await this._request({
            method: 'PUT',
            url: `/api/budget/${year}/category/${categoryId}`,
            data: entry
        })
    },

    /**
     * @param {number} year
     * @param {string} categoryId
     * @returns {Promise<Result>}
     */
    async deleteBudgetEntry(year, categoryId) {
        return await this._request({
            method: 'DELETE',
            url: `/api/budget/${year}/category/${categoryId}`,
        })
    },

    /**
     * @param {dayjs.Dayjs} from
     * @param {dayjs.Dayjs} to
     * @returns {Promise<Result>}
     */
    async getPlannedTransactions(from, to) {
        return await this._request({
            method: 'GET',
            url: `/api/budget/planned_transactions/${from.format('YYYY-MM-DD')}/to/${to.format('YYYY-MM-DD')}`
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// CHART CONTROLLER
    ///

    /**
     * @param beginDate {Date}
     * @param endDate {Date}
     * @returns {Promise<Result>}
     */
    async getSpendingByCategoryForPeriod(beginDate, endDate) {
        let from = beginDate.toLocaleDateString('fr-CA')
        let to = endDate.toLocaleDateString('fr-CA')
        return await this._request({
            method: 'GET',
            url: `/api/charts/spending?from=${from}&to=${to}`,
        })
    },

    /**
     * @param {Date} from
     * @param {Date} to
     * @param {string} period
     * @returns {Promise<Result>}
     */
    async getBarChart(from, to, period) {
        let fromDate = from.toLocaleDateString('fr-CA')
        let toDate = to.toLocaleDateString('fr-CA')
        return await this._request({
            method: 'GET',
            url: `/api/charts/bar/${fromDate}/${toDate}/${period}`
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// ACCOUNT CONTROLLER
    ///

    /**
     * @returns {Promise<Result>}
     */
    async getAccountBalance() {
        return await this._request({
            method: 'GET',
            url: `/api/account/balance`,
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// TASK CONTROLLER
    ///

    /**
     * @returns {Promise<Result>}
     */
    async getWebScraperStatus() {
        return await this._request({
            method: 'GET',
            url: `/api/task`,
        })
    },

    /**
     * @param interval {number}
     * @returns {Promise<Result>}
     */
    async startWebScraper(interval) {
        return await this._request({
            method: 'GET',
            url: `/api/task/start?interval=${interval}`,
        })
    },

    /**
     * @returns {Promise<Result>}
     */
    async stopWebScraper() {
        return await this._request({
            method: 'GET',
            url: `/api/task/stop`,
        })
    },

    /**
     * @param args {string[]}
     * @returns {Promise<Result>}
     */
    async executeWebScraper(args) {
        return await this._request({
            method: 'POST',
            url: `/api/task/execute`,
            data: args,
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// UTILITIES CONTROLLER
    ///

    /**
     * @returns {Promise<Result>}
     */
    async getAllClimbingSessions() {
        return await this._request({
            method: 'GET',
            url: '/api/utilities/climbing',
        })
    },

    /**
     * @param {{ date: Date, duration: number, grades: string, comment: string }} sessionData
     * @returns {Promise<Result>}
     */
    async saveClimbingSession(sessionData) {
        return await this._request({
            method: 'POST',
            url: '/api/utilities/climbing',
            data: sessionData,
        })
    },

    /**
     * @param {string} sessionId
     * @returns {Promise<Result>}
     */
    async deleteClimbingSession(sessionId) {
        return await this._request({
            method: 'DELETE',
            url: `/api/utilities/climbing/${sessionId}`,
        })
    },


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// SEARCH CONTROLLER
    ///

    /**
     * @param {string} query
     * @param {number} [size]
     * @param {number} [skip]]
     * @returns {Promise<Result>}
     */
    async searchTransaction(query, size = 10, skip = 0) {
        return await this._request({
            method: 'GET',
            url: `/api/search/transaction?search=${query}&size=${size}&skip=${skip}`,
        })
    },

}
