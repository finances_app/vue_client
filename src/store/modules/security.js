import securityApi from '../../api/security'

export default {
    namespaced: true,
    // The shape of the state
    state: () => ({
        accessToken: null,
        refreshToken: null,
        tokenExpiration: null,
        currentUser: null
    }),

    // functions to get data from the state
    getters: {
        loginSuccess(state) {
            return state.currentUser !== null
        }
    },

    // async api calls are done here. Local data is changed using mutators
    actions: {
        async loginOAuth({ commit }) {
            // TODO implement cordova Google signin
        },

        async loginPassword({ commit }, { email, password }) {
            commit('setLoggedInUser', null)

            const result = await securityApi.passwordLogin(email, password)

            if (result.isSuccess) {
                commit('updateTokens', result.body)
                // FIXME: the server will return the user infos when the password login is implemented
                commit('setLoggedInUser', {
                    familyName: 'client',
                    givenName: 'DEV',
                    imageUrl: 'assets/app-icon.png',
                })
            } else if (result.hasValidationError) {
                commit('showSnackbar', { message: result.validationError }, {root: true})
            } else if (result.isNetworkError) {
                commit('setOnlineState', false, { root: true })
            } else {
                commit('showSnackbar', {
                    message: `[${result.status}] ${result.error.message}`,
                    isError: true
                    }, { root: true })
            }
        },

        async silentAuth({ commit }) {
            // TODO implement cordova Google signin
        }
    },

    // functions changing the state
    mutations: {
        updateTokens(state, { access_token, expires, expires_in, refresh_token }) {
            state.accessToken = access_token
            state.refreshToken = refresh_token

            let serverTime = expires - expires_in
            let deviceTime = Date.now()
            let difference = deviceTime - serverTime
            state.tokenExpiration = expires + difference
        },

        setLoggedInUser(state, user) {
            state.currentUser = user
        }
    }
}
