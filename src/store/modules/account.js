import dataApi from '../../api/data'

export default {
    namespaces: true,

    // The shape of the state
    state: {

    },

    // functions to get data from the state
    getters: {

    },

    // async api calls are done here. Local data is changed using mutators
    actions: {

    },

    // functions changing the state
    mutations: {

    }
}
