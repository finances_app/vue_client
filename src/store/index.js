import Vue from 'vue'
import Vuex from 'vuex'
import security from './modules/security'
import account from './modules/account'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        security,
        account,
    },
    strict: debug,

    state: () => ({
        snackbar: {
            message: null,
            open: false,
            isError: false,
            actionLabel: null
        },
        isOnline: true
    }),

    mutations: {
        showSnackbar(state, { message, isError = false, actionLabel = null }) {
            state.snackbar = {
                message,
                isError,
                actionLabel,
                open: true
            }
        },

        setOnlineState(state, isOnline) {
            state.isOnline = isOnline
        }
    }
})
