import Vue from 'vue'
import Router from 'vue-router'
import LoginView from '../views/Login'
import MainLayout from '../layouts/Main'
import DashboardView from '../views/Dashboard'
import OrganizeView from '../views/Organize'
import PlaningView from '../views/Planing'
import JobsView from '../views/Jobs'

Vue.use(Router)

export default new Router({
    routes: [
        { path: '/', redirect: '/login' },
        { path: '/login', component: LoginView },
        { path: '/app', component: MainLayout,
          children: [
              { path: '', component: DashboardView, name: 'dashboard' },
              { path: 'organize', component: OrganizeView, name: 'organize' },
              { path: 'planing', component: PlaningView, name: 'planing' },
              { path: 'jobs', component: JobsView, name: 'jobs' },
          ]
        }
    ],
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }
})
