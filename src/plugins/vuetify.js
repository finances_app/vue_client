import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: '#8bc34a',
                secondary: '#ccbf33',
                error: '#cf6679',
            },
        },
    },
});
